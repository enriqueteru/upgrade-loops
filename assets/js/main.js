window.onload = ()=>{


//Iteración #1: Usa includes**

const products = ['Camiseta de Pokemon', 'Pantalón coquinero', 'Gorra de gansta', 'Camiseta de Basket', 'Cinturón de Orión', 'AC/DC Camiseta']


const encontrarCamiseta = (arr) => {
const camisetafind = [];

        for(item of arr){
        if(item.includes('Camiseta')) {
        camisetafind.push(item);
        console.log(` ${item} llega la palabra "camiseta" `);
    };
 };

 return camisetafind;

};

encontrarCamiseta(products);






//Iteración #2: Condicionales avanzados


const alumns = [
    {name: 'Pepe Viruela', T1: false, T2: false, T3: true}, 
       {name: 'Lucia Aranda', T1: true, T2: false, T3: true},
       {name: 'Juan Miranda', T1: false, T2: true, T3: true},
       {name: 'Alfredo Blanco', T1: false, T2: false, T3: false},
       {name: 'Raquel Benito', T1: true, T2: true, T3: true}
]


alumns.forEach(alumno => {
   if((alumno.T1 + alumno.T2 + alumno.T3) >= 2){
       alumno.isApproved = true;
   }else{
       alumno.isApproved = false;
   }
   console.table(alumno);
});


/* Método alternativo

const alumniAprovado = (arr) => {

    for(alumno of arr){
  
      if (alumno.T1 === true && alumno.T3 === true) {
          alumno.isApproved = true;
  
      } else if (alumno.T1 === true && alumno.T2 === true) {
          alumno.isApproved = true;
  
      } else if (alumno.T3 === true && alumno.T2 === true) {
          alumno.isApproved = true;
  
      } else {
          alumno.isApproved = false;
      }};
  
    console.table(alumns);
  };
   */

/* 
//Iteración #3: destinos(placesToTravel)

  const placesToTravel = ['Japon', 'Venecia', 'Murcia', 'Santander', 'Filipinas', 'Madagascar'];

  const destinos =  arr => {

  for (place of arr){
      console.log(place);
  }};

  destinos(placesToTravel);

 */

//Iteración #4: Probando For...in

const alien = {
    name: 'Wormuck',
    race: 'Cucusumusu',
    planet: 'Eden',
    weight: '259kg'
}

const persona = {
    name: 'Joe',
    race: 'Human',
    planet: 'Yupi',
    weight: '109kg'
}

const printAlien = obj => {

    for (value in obj){
    console.log( value + ': ' + obj[value]);
    }

};

printAlien(alien);
printAlien(persona);



//Iteración #5: Probando For

const placesToTravel = [
    {id: 5, name: 'Japan'}, 
    {id: 11, name: 'Venecia'}, 
    {id: 23, name: 'Murcia'}, 
    {id: 40, name: 'Santander'}, 
    {id: 44, name: 'Filipinas'}, 
    {id: 59, name: 'Madagascar'}
];


const travelPlaces = arr => {

    for( i = 0 ; i < arr.length; i++){
     console.log(arr[i].name);
     if(arr[i].id === 11 || arr[i].id === 40){
        arr.pop(arr[i]);
     }};

     console.table(arr);
};


travelPlaces(placesToTravel);



//Iteración #6: Mixed For...of e includes

const toys = [
    {id: 5, name: 'Buzz MyYear'},
    {id: 11, name: 'Action Woman'},
    {id: 23, name: 'Barbie Man'},
    {id: 40, name: 'El gato'},
    {id: 40, name: 'gato'}
    ];



const catToys = arr => {

    for(toy of arr){

        
        if (toy.name.includes('gato') && toy.id === 40){
            arr.pop(toy);
        }
        console.log(toy);
    }

}; 

catToys(toys);





//Iteración #7: For...of avanzado


const popularToys = [];
const toys = [
	{id: 5, name: 'Buzz MyYear', sellCount: 10}, 
	{id: 11, name: 'Action Woman', sellCount: 24}, 
	{id: 23, name: 'Barbie Man', sellCount: 15}, 
	{id: 40, name: 'El gato con Guantes', sellCount: 8},
	{id: 40, name: 'El gato felix', sellCount: 35}
]


const allPopularToys = (arr) => {
    for(toy of arr){
        if(toy.sellCount >= 15){
        popularToys.push(toy);
    };

  };
  console.table(popularToys);
};

allPopularToys(toys);

















}